<?php
$conn = new mysqli('localhost','root','','films');
if($conn->connect_error){
    die("Помилка підключення!:" .$conn->connect_error);
}
$conn->query("SET CHARSET utf8");
$sql = "SELECT `id`,`name`,`year`, `isActive` FROM films WHERE `isActive` = 1";
$result = $conn->query($sql);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Перегляд списку фільмів</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <header>
        </header>
        <table cellspacing="0" cellpadding="5" border="1">
            <tr>
                <th>Назва фільму:</th>
                <th>Рік прем’єри</th>
            </tr>
        <?php
            if($result->num_rows > 0){
                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    echo '<tr><td>'.$row['name'].'</td><td>'.$row['year'].'</td></tr>';
                }
            }
            else{
                echo "Немає даних для відображення.";
            }
            $conn->close();
        ?>
        </table>
        <a href="add.php">Додати фільм</a>
        <footer>  
        </footer>
    </body>
</html>