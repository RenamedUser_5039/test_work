<?php
// Create connection
$conn = new mysqli('localhost','root','','films');
// Check connection
if($conn->connect_error){
    die("Connection failed: " .$conn->connect_error);
} 
// sql to create table
$sql = "CREATE TABLE films(
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(100) NOT NULL,
        year INT(4) UNSIGNED NOT NULL,
        isActive tinyint(1) default TRUE)";
if($conn->query($sql) === TRUE){
    echo "Table films created successfully";
}
else{
    echo "Error creating table: " . $conn->error;
}
$conn->close();
?>

