<?php
if(isset($_POST) && !empty($_POST)){
    //присвоюэмо значення опублікованості
    if(isset($_POST['check'])){
        $check = 1; 
    }
    else{
        $check = 0; 
    }
    //перевірка заповненості обов’язкових полів
    if(empty($_POST['film_title']) || empty($_POST['film_year'])){
        exit("Не всі поля були заповнені!");
    }
    //фільтруємо дані користувача
    $film_title = trim(htmlspecialchars(stripslashes($_POST['film_title'])));
    $film_year = (int)$_POST['film_year'];
    //валідація даних року
    if($film_year < 1000 || $film_year > 2999){
        exit("Введений рік не є коректним!");
    }
    //валідація на довжину назви
    if(strlen($film_title) > 100 or strlen($film_title) < 1){
        exit("Недопустима кількість символів в назві!");
    }
    //підключення до б.д.
    //звичайно, можна винести підключення до б.д. в окремий файл
    $conn = new mysqli('localhost','root','','films');
    if($conn->connect_error){
        die("Помилка підключення!:" .$conn->connect_error);
    }
    $conn->query("SET CHARSET utf8");
    $conn->query("INSERT INTO `films` (`name`, `year`, `isActive`) VALUES ('{$film_title}',{$film_year},{$check})");
    $conn->close();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Додати фільм</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <header>
            
        </header>
        <form method="POST" action="add.php">
            <p>
                <input required="required" placeholder="Назва фільму" type="text" name="film_title" maxlength="100"><br><br>
                <input required="required" placeholder="Рік прем’єри" type="text" name="film_year" maxlength="4"><br><br>
                <input type="checkbox" checked="checked" name="check">Опубліковано<br><br>
                <input type="submit" value="Додати">
            </p>
        </form>
        <a href="index.php">Назад</a>
        <footer>
            
        </footer>
    </body>
</html>

